# Philibert

Based of the lettering of old road signs in France for hamlets and localities. This lettering is also in use as clean vectors on some motorway signs in Luxembourg.

## Content of the directory

To do

## Production

Roughly vectorised in the 90's.

## Known issues and future developments

Very small glyphset, chuncky spacing, ideally must have lot of different versions of each glyphs.

## Coverage

To do

<h2>Information for Contributors</h2>

Philibert is released under the OFL 1.1 - http://scripts.sil.org/OFL
For information on what you're allowed to change or modify, consult the ofl.txt and ofl-faq.txt files. The ofl-faq also gives a very general rationale and various recommendations regarding why you would want to contribute to the project or make your own version of the font.

## ChangeLog

(This should list both major and minor changes, most recent first.)

To do

## Acknowledgements

If you make modifications be sure to add your name (N), email (E), web-address
(W) and description (D). This list is sorted by last name in alphabetical order.

N: Pierre Huyghebaert <br>
E: pierre@speculoos.com <br>
W: http://www.speculoos.com <br>
D: Typography

## Historical background

To do
